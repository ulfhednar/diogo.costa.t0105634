import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import MoviesCreate from '@/components/MoviesCreate'
import MoviesIndex from '@/components/MoviesIndex'
import MoviesYear from '@/components/MoviesYear'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/movies/create',
      name: 'movies.create',
      component: MoviesCreate
    },
    {
      path: '/movies/index',
      name: 'movies.index',
      component: MoviesIndex
    },
    {
      path: '/movies/year',
      name: 'movies.year',
      component: MoviesYear
    }

  ]
})
